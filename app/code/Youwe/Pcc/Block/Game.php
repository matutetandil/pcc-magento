<?php
/**
 * @author mdenda
 * @date 2019-08-31
 */

namespace Youwe\Pcc\Block;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;

class Game extends Template
{
    /**
     * @var Registry
     */
    protected $registry;

    public function __construct(
        Template\Context $context,
        Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->registry = $registry;
    }

    public function getNewDeckUrl()
    {
        return $this->getUrl('pcc/deck/new');
    }

    public function getNewCardUrl($card)
    {
        return $this->getUrl('pcc/deck/card', ['card' => $card]);
    }

    public function getUserCard()
    {
        return $this->registry->registry('card');
    }

    public function getNewGameUrl()
    {
        return $this->getUrl('pcc');
    }
}
