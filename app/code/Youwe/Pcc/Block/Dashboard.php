<?php
/**
 * @author mdenda
 * @date 2019-08-31
 */

namespace Youwe\Pcc\Block;

use Magento\Framework\View\Element\Template;
use Youwe\Pcc\Model\Deck;
use Youwe\Pcc\Model\DeckFactory;

class Dashboard extends Template
{
    /**
     * @var Deck
     */
    protected $deck;

    public function __construct(
        Template\Context $context,
        DeckFactory $deckFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->deck = $deckFactory->create();
    }

    public function getDeckCards()
    {
        return $this->deck->getAllCards();
    }

    public function getGameUrl()
    {
        return $this->getUrl('pcc/deck/game');
    }
}
