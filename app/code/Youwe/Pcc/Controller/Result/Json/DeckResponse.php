<?php
/**
 * @author mdenda
 * @date 2019-08-31
 */

namespace Youwe\Pcc\Controller\Result\Json;

use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Translate\InlineInterface;

class DeckResponse extends Json
{
    protected $arrayResponse;

    public function __construct(
        InlineInterface $translateInline,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null
    ) {
        parent::__construct($translateInline, $serializer);

        $this->arrayResponse = [];
    }

    public function setMessage($message)
    {
        $this->arrayResponse['message'] = $message;
        return $this;
    }

    public function setResult($response)
    {
        $this->arrayResponse['result'] = $response;
        return $this;
    }

    public function setCard($card)
    {
        $this->arrayResponse['card'] = $card;
        return $this;
    }

    public function setCardsLeft($left)
    {
        $this->arrayResponse['left'] = $left;
        return $this;
    }

    public function getResponse()
    {
        return $this->setData($this->arrayResponse);
    }
}
