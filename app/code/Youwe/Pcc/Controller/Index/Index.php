<?php

/**
 * @author mdenda
 * @date 2019-08-31
 */

namespace Youwe\Pcc\Controller\Index;

use Magento\Framework\App\Action\Action;

class Index extends Action
{

    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
