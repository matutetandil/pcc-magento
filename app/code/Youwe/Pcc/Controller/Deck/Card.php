<?php

/**
 * @author mdenda
 * @date 2019-08-31
 */

namespace Youwe\Pcc\Controller\Deck;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Session\SessionManagerInterface;
use Youwe\Pcc\Controller\Result\Json\DeckResponse;
use Youwe\Pcc\Controller\Result\Json\DeckResponseFactory;
use Youwe\Pcc\Model\Deck;

class Card extends Action
{
    /**
     * @var SessionManagerInterface
     */
    protected $sessionManager;
    /**
     * @var DeckResponseFactory
     */
    protected $deckResponseFactory;

    public function __construct(
        Context $context,
        DeckResponseFactory $deckResponseFactory,
        SessionManagerInterface $sessionManager
    ) {
        parent::__construct($context);
        $this->sessionManager = $sessionManager;
        $this->deckResponseFactory = $deckResponseFactory;
    }

    public function execute()
    {
        $this->sessionManager->start();

        $userCard = $this->_request->getParam('card');

        /** @var Deck $deck */
        $deck = $this->sessionManager->getDeck();

        $card = $deck->getCard();
        $cardsLeft = $deck->getTotalCardsLeft();

        /** @var DeckResponse $response */
        $response = $this->deckResponseFactory->create();

        $response
            ->setCard($card)
            ->setCardsLeft($cardsLeft);

        if ($userCard == $card) {
            $chance = round(1/($cardsLeft + 1) * 100, 4);

            $response
                ->setMessage("Got it, the chance was {$chance}%")
                ->setResult('Ok');
        } else {
            $chance = round(1/$cardsLeft * 100, 4);
            $response
                ->setMessage($chance . '%')
                ->setResult('No');
        }

        $response->setData($response);

        return $response->getResponse();
    }
}
