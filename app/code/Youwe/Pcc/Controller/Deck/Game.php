<?php

/**
 * @author mdenda
 * @date 2019-08-31
 */

namespace Youwe\Pcc\Controller\Deck;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Registry;

class Game extends Action
{
    /**
     * @var Registry
     */
    protected $registry;

    public function __construct(
        Context $context,
        Registry $registry
    ) {
        parent::__construct($context);
        $this->registry = $registry;
    }

    public function execute()
    {
        $this->registry->register('card', $this->_request->getParam('card'));

        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
