<?php

/**
 * @author mdenda
 * @date 2019-08-31
 */

namespace Youwe\Pcc\Controller\Deck;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Session\SessionManagerInterface;
use Youwe\Pcc\Controller\Result\Json\DeckResponse;
use Youwe\Pcc\Controller\Result\Json\DeckResponseFactory;
use Youwe\Pcc\Model\DeckFactory;

class NewAction extends Action
{
    /**
     * @var DeckFactory
     */
    protected $deckFactory;
    /**
     * @var SessionManagerInterface
     */
    protected $sessionManager;
    /**
     * @var DeckResponseFactory
     */
    protected $deckResponseFactory;

    public function __construct(
        Context $context,
        SessionManagerInterface $sessionManager,
        DeckFactory $deckFactory,
        DeckResponseFactory $deckResponseFactory
    ) {
        parent::__construct($context);
        $this->deckFactory = $deckFactory;
        $this->sessionManager = $sessionManager;
        $this->deckResponseFactory = $deckResponseFactory;
    }

    public function execute()
    {
        $deck = $this->deckFactory->create();
        $deck->shuffleDeck();

        $this->sessionManager->start();
        $this->sessionManager->setDeck($deck);

        /** @var DeckResponse $response */
        $response = $this->deckResponseFactory->create();
        $response->setResult('OK');

        return $response->getResponse();
    }
}
