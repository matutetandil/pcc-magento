<?php
/**
 * @author mdenda
 * @date 2019-08-31
 */

namespace Youwe\Pcc\Test\Unit;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Youwe\Pcc\Model\Deck;

class CalculatorTest extends TestCase
{
    /**
     * @var Deck
     */
    protected $sampleClass;
    /**
     * @var float
     */
    protected $expectedResult;
    /**
     * @var array
     */
    protected $cards;
    /**
     * @var Deck
     */
    private $sampleClass2;

    public function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->sampleClass = $objectManager->getObject('Youwe\Pcc\Model\Deck');
        $this->sampleClass2 = $objectManager->getObject('Youwe\Pcc\Model\Deck');
        $this->expectedResult = round(1/45, 4);

        foreach (['H', 'S', 'D', 'C'] as $suit) {
            for ($i = 1; $i <= 13; $i++) {
                switch ($i) {
                    case 1:
                        $rank = 'A';
                        break;
                    case 11:
                        $rank = 'J';
                        break;
                    case 12:
                        $rank = 'Q';
                        break;
                    case 13:
                        $rank = 'K';
                        break;
                    default:
                        $rank = $i;
                }
                $this->cards[] = "$suit$rank";
            }
        }
    }

    public function testChanceCalculation()
    {
        // Shuffle the Deck
        $this->sampleClass->shuffleDeck();

        // Remove 5 cards
        $this->sampleClass->getCard();
        $this->sampleClass->getCard();
        $this->sampleClass->getCard();
        $this->sampleClass->getCard();
        $this->sampleClass->getCard();

        $this->assertEquals($this->expectedResult, round(1/$this->sampleClass->getTotalCardsLeft(), 4));
    }

    public function testGeneratedDeck()
    {
        // Shuffle the Deck
        $this->sampleClass2->shuffleDeck();

        while ($this->sampleClass2->getTotalCardsLeft() != 0) {
            $this->cards = array_diff($this->cards, [$this->sampleClass2->getCard()]);
        }

        $this->assertEquals(0, sizeof($this->cards));
    }
}
