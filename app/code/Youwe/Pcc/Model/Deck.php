<?php
/**
 * @author mdenda
 * @date 2019-08-31
 */

namespace Youwe\Pcc\Model;

use Magento\Framework\Model\AbstractModel;

class Deck extends AbstractModel
{
    protected $cards;

    protected function _construct()
    {
        parent::_construct();

        foreach (['H', 'S', 'D', 'C'] as $suit) {
            for ($i = 1; $i <= 13; $i++) {
                switch ($i) {
                    case 1:
                        $rank = 'A';
                        break;
                    case 11:
                        $rank = 'J';
                        break;
                    case 12:
                        $rank = 'Q';
                        break;
                    case 13:
                        $rank = 'K';
                        break;
                    default:
                        $rank = $i;
                }
                $this->cards[] = "$suit$rank";
            }
        }
    }

    public function getAllCards()
    {
        return $this->cards;
    }

    public function shuffleDeck()
    {
        shuffle($this->cards);
        return $this;
    }

    public function getCard()
    {
        return array_shift($this->cards);
    }

    public function getTotalCardsLeft()
    {
        return sizeof($this->cards);
    }
}
